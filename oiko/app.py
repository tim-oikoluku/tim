# import redis
from flask import Flask, request, make_response
from libvoikko import Voikko, Sentence, GrammarError, Token
from html.parser import HTMLParser
import json

# Instance of Flask framework
app = Flask(__name__)

# Redis
# cache = redis.Redis(host='redis', port=6379)

# Instance of Voikko (Linquistic tool for Finnish)
v = Voikko("fi")

'''
Luokka, jonka jäsenfunktioissa käsitellään html:n parsiminen
Vähän toiminnallisuudesta:
Luokka rakentaa alkuperäisen html:n uudelleen (jäsenmuuttuja html) iteroimalla
alkuperäistä html:ää samalla merkiten mahdolliset virheet iteroinnin varrella.
Muutkin lähestymistavat mahdollisia.
'''


class HtmlParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.currentTag = ''
        self.currentClass = ''
        self.bannedTags = ['code']
        self.bannedClasses = ['math inline']
        self.words = []
        self.html = ''
        self.errors = 0

    # Syötetään html yläluokalle parsittavaksi
    def feed(self, data):
        self.words = []
        self.currentTag = ''
        self.currentClass = ''
        self.html = ''
        self.errors = 0

        HTMLParser.feed(self, data)
        return self.words, self.html

    # Aloitustägeihin liittyvä toiminnallisuus
    def handle_starttag(self, tag, attrs):
        self.currentTag = tag
        attrdata = ''
        for attr in attrs:
            if attr[0] == 'class':
                self.currentClass = attr[1]
            attrdata += f' {attr[0]}=\"{attr[1]}\"'
            print(attrdata)

        self.html += f'<{tag}{attrdata}>'

    # Lopetustägeihin liittyvä toiminnallisuus
    def handle_endtag(self, tag):
        self.currentTag = ''
        self.currentClass = ''
        self.html += str('</' + tag + '>')

    # Varsinaiseen tekstisisältöön liittyvä toiminnallisuus
    def handle_data(self, data):
        startpos = self.getpos()[1]
        currentpos = startpos
        if self.currentTag in self.bannedTags or self.currentClass in self.bannedClasses:
            self.html += data
            return
        elif data == '\n':
            return
        else:
            spell_checked_words = []
            tokenized_words = v.tokens(data)
            for word in tokenized_words:
                checked_word = {}
                if word.tokenType == Token.WORD and v.spell(word.tokenText) is False:
                    self.errors += 1
                    checked_word['word'] = word.tokenText
                    checked_word['id'] = self.errors
                    checked_word['suggestions'] = v.suggest(word.tokenText)
                    checked_word['error'] = True
                    spell_checked_words.append(checked_word)
                else:
                    checked_word['word'] = word.tokenText
                    checked_word['error'] = False
                    spell_checked_words.append(checked_word)

            for word in spell_checked_words:
                newword = word
                newword['start'] = currentpos
                newword['line'] = self.getpos()[0]
                currentlength = len(word['word'])
                newword['length'] = currentlength
                currentpos += currentlength
                if word['error']:
                    self.words.append(newword)
                    self.html += f"<u id={newword['id']}>{newword['word']}</u> "
                    # self.html += f"<spell-error>{newword['word']}</spell-error>"
                else:
                    self.html += f"{newword['word']} "


    # Tästä kutsuttaisiin varmaankin oikolukuun liittyviä funktioita
    # Palauttaa merkkijonon, jossa virheet on tägätty


#   def mark_errors(self, data):
#        return data

parser = HtmlParser()



def spell_check_words(query_string):
    '''
    Iterates through words and for every incorrect spelled word, it returns a list of 
    dictionaries that contains:
    The word
    List of suggestions to replace the word
    '''

    spell_checked_words = []

    #Mitä tämä allaoleva kohta tykkää html-elementeistä?
    tokenized_words = v.tokens(query_string)

    for word in tokenized_words:
        if word.tokenType == Token.WORD and v.spell(word.tokenText) is False:
            checked_word = {}
            checked_word['word'] = word.tokenText
            checked_word['suggestions'] = v.suggest(word.tokenText)
            spell_checked_words.append(checked_word)
    return spell_checked_words


def grammar_check_sentences(query_string):
    '''
    Iterates sentences and for every sentence with grammar error, it returns a list 
    of dictionaries that includes:
    The sentence
    List of grammar errors.
    '''

    sentences = v.sentences(query_string)
    grammar_checked_sentences = []
    for sentence in sentences:
        grammar_error_objects = v.grammarErrors(sentence.sentenceText, 'fi')
        if grammar_error_objects != []:
            grammar_checked_sentence = {}
            grammar_checked_sentence['sentence'] = sentence.sentenceText
            grammar_checked_sentence['grammar_errors'] = []
            for grammar_error_object in grammar_error_objects:
                #TODO: Allaoleva esitys lauseen virheistä ei ole toimiva, vaatii paremman ratkaisun
                grammar_checked_sentence['grammar_errors'].append(grammar_error_object.toString())
            grammar_checked_sentences.append(grammar_checked_sentence)
    return grammar_checked_sentences


def generate_response_body(query_string):
    '''
    Generates a response body that includes proofreading data related to words and sentences.
    Returns this data as json.
    '''
    response_body = {}
    # response_body['words'] = spell_check_words(query_string)
    response_body['words'], response_body['htmldata'] = parser.feed(query_string)
    parser.reset()

    # response_body['sentences'] = grammar_check_sentences(query_string)
    return json.dumps(response_body, indent=4)


@app.route('/')
def hello():
    '''
    Route to main page
    '''
    return 'This is a Finnish proofreading service that uses Voikko-library.\n'


@app.route('/api/v1/proofread')
def proofread():
    '''
    Route to proofread
    '''
    response_body = generate_response_body(request.args.get('text'))
    headers = {"Content-Type": "application/json"}
    return make_response(response_body, 200, headers)

@app.route('/api/v1/proofread/add_word', methods=['POST'])
def add_word(word):
    '''
    Route for adding a word to dictionary
    '''
    # TODO: Check if word is valid
    # TODO: Check if word excist in a dictionary allready
    # TODO: Save word
    # TODO: Return a response that saving was successful
