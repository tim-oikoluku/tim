timApp.korppi package
=====================

Submodules
----------

timApp.korppi.korppimock module
-------------------------------

.. automodule:: timApp.korppi.korppimock
    :members:
    :undoc-members:
    :show-inheritance:

timApp.korppi.openid module
---------------------------

.. automodule:: timApp.korppi.openid
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.korppi
    :members:
    :undoc-members:
    :show-inheritance:
