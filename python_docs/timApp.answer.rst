timApp.answer package
=====================

Submodules
----------

timApp.answer.answer module
---------------------------

.. automodule:: timApp.answer.answer
    :members:
    :undoc-members:
    :show-inheritance:

timApp.answer.answer\_models module
-----------------------------------

.. automodule:: timApp.answer.answer_models
    :members:
    :undoc-members:
    :show-inheritance:

timApp.answer.answers module
----------------------------

.. automodule:: timApp.answer.answers
    :members:
    :undoc-members:
    :show-inheritance:

timApp.answer.pointsumrule module
---------------------------------

.. automodule:: timApp.answer.pointsumrule
    :members:
    :undoc-members:
    :show-inheritance:

timApp.answer.routes module
---------------------------

.. automodule:: timApp.answer.routes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.answer
    :members:
    :undoc-members:
    :show-inheritance:
