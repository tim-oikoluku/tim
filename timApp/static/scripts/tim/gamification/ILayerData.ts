export interface ILayerData {
    [index: string]: number;
}
