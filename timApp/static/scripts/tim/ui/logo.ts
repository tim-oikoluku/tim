import {timApp} from "../app";

timApp.component("timLogo", {
    template: `
<img alt="TIM - The Interactive Material" class="pull-left fullheight"
     src="/static/images/tim-logo.svg"
     title="TIM"/>
`,
});
