import {timApp} from "../app";

timApp.component("timLoading", {
    template: `<i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>`,
});
