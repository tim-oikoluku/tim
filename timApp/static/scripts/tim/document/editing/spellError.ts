// Tässä toteutetaan virheen merkkaava ja korjausehdotukset näyttävä elementti

import {IController} from "angular";
import {timApp} from "../../app";

class SpellErrorCtrl implements IController {
    async $onInit() {
    }
}

timApp.component("spellError", {
    controller: SpellErrorCtrl,
    transclude: true,
    template: `<u><ng-transclude></ng-transclude></u>`,
});
