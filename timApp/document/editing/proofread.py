import requests
from flask import Response
import json
import urllib.parse

# Returns proofread-object
def proofread_pars(pars):

    '''Tässä tehdään kutsu oiko-konttiin. Tuo request-funktio palauttaa flaskin Response-objektin.
    Tässä vaiheessa oikolukukonttiin lähetetään siis vain tuo kovakoodattu tekstinpätkä,
    tarkoitus olisi, että kyseinen haku tehtäisiin pars-muuttujan sisältämistä teksteistä.
    Tuo pars sisältää taulukon, jossa on objekteja, jotka sisältävät mm. paragraphien tekstisisällön,
    markdownin ja html:n kissa `istuu` <- koodin merkintä'''

    # Url-enkoodaus
    url = "http://oiko:5000/api/v1/proofread"
    text = pars[0]['html']
    response = requests.get(url, params={'text': text})

    # Tässä palautetaan Response-objektin tekstimuotoinen versio jsoniksi muunnettuna
    return json.loads(response.text)


